CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration


INTRODUCTION
-------

The Domain Video Sitemap module. Provides possibility to create video sitemap
specific to each domain.
It Manages both videos uploaded to nodes and video added in youtube field. 
Videos added to block or other entity types are not indexed.
  
  * More information on domain video sitemaps can be found at 
    https://developers.google.com/webmasters/videosearch/sitemaps

REQUIREMENTS
------------

This module requires the following modules:
    
  * File Entity (https://www.drupal.org/project/file_entity)

INSTALLATION
-------

Simply install Domain Video Sitemap like you would any other module.
  * Copy the domain_video_sitemap folder to the modules folder in your 
    installation.
  * Enable the module using Administer -> Modules (/admin/modules).

CONFIGURATION
-------------

Configuration page link is /admin/config/search/video-sitemap.
  * Select the content type which you want to include in sitemap. 
  * Set Cache timeout (minutes) of sitemap.
  * You can exclude file mimetype from indexing if you don't need it to be in 
    the sitemap.
  * Select the source of video to include in sitemap. For example you want 
    sitemap only including youtube fields or videos uploaded in nodes. 
